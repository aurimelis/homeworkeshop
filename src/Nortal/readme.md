﻿# Komentarai

## Bendrai

Bendrai programuodamas ir projektuodamas sita produkta laikiausi nuomones kad tai bus nedidelis appsas ir skirtas tik parodyti kad moku programuoti. 
Del to viskas padaryta gana paprastai, del laiko trukumo ir remiantis tuo kad tai ne reali applikacija keletoje vietu pasirinkau paprastesnius kelius, kad galeciau uzbaigti produkta ir jis veiktu.
Tas vietas siek tiek ir aprasysiu siame doke. Jei kiltu kokiu neaiskumu, kodel dariau taip o ne kitaip, tai drasiai galite klausti pries pramesdami, manau kad turesiu tam atsakyma :)
Su stiliais ir naujais feature taip pat nepersistengiau del laiko trukumo.


## Duombaze

Pagal duota json faila galima interpretuoti duombaze keliais budais. Priklausomai nuo to kiek giliai norime ziureti i sita appsa kaip i eshopa. 
Jei ziureti i tai kaip i testini projektuka, ar labai maza eshopa, kur duomenys nesikeist, ir nesiples tai paprasciausias budas tureti viena lentele su visais laukais. Bet nesirinkau sito varianto nes atrodytu labai jau weak.
Pasirinkau pasidaryti 3 lenteles, kurios atspindetu duota json failiuka. Cia sakyciau toks vidutinis sprendimas, nes jis nera taip jau elementariai prapleciamas. Reiktu prideti lauku, pakeisti koda. Bet manau kad tokiam parodomajam variantui tiks.
Jei noretume daryti pilnai veikianti eshope tuomet DB struktura daryciau sudetingesne ir dinamiskesne. ProductPpecs lenta pasidalintu i kelias, kuriose buvu duomenys butu saugomi Key Value pavidalu. 
Pvz Galima butu tureti Specs lenta, kurioje butu visi imano specsai ('manufacture','os',etc.), SpecsValues (SpecsId,Value) lenta, kurioje butu konkretaus specso values ('manufacturer':'nokia'). Tuomet cia butu visos imanomos specsu kombinacijos.
Ir galime tureti mapinimo lenta ProductSpecs (ProductId, SpecsValueId) kurioje butu konkreciu produktu konkretus specsai. Tokiu atveju galetume lengvai isvedineti filtrus i clientside is specs lentos. Ir labai lengvai filtruotusi.
Kiek sunkiau butu su nauju produktu insertu ir updateu, kaskart reiktu tikrinti ar toks scpes jau yra, ar prideti nauja ir priskirti produktui ir t.t
Su images lenta pasirinkau siek tiek kita approach, kad butu galima lengvai prideti nauja image size, nereiktu nauju stulpeliu, uztektu prisidet enum reiksme.
Tai del DB atrodo tiek norejau pasakyt.


## Projekto architektura, paternai

Pasirinkau naudoti .net core, nes norejau parodyti kad nesu pastriges kazkokioj senovej, ir naujoves man nera svetimos, tikiuosi kad pavyks jums pasileisti ji :) 
Dar buvau susimastes daryti SPA applikacija, nes issisprestu keletas klausimu su filtravimais ir siaip butu tvarkingesne applikacija, bet tam butu prireike tikrai daugiau laiko, tai dariau su paprastu mvc ir ajax callais.
Nezinau ar reikejo cia enterprise applikacijos, tai neskaidziau solutiono i kelis projektus, kuriuose butu data access layeris, bussiness logika, ir presentation layeris.
Del to viska dariau vienam projekte, stengdamasis jame atskirti tam tikrus layerius. Zinoma jei tai butu multiprojektinis solutionas, tai galima butu projektuoti kitaip.
Siai applikacijai pasirinkau naudoti Services layer, nors yra manoma kad servisai skirti tik bussiness logikai ir nereikia juose kreiptis i DB, taciau manau kad esant nedidelei applikacijai, kur nera daug biznio logikos ir nenaudojamas repository paternas galima kreiptis i DB ir servisuose.
Kitu atveju galima butu naudoti toki paterna: _Controller_ -> _Service_ -> _Repository_. Kur controller butu atsakingas tik komunikacijai su client side, servisas visai biznio logikai o repositorija tik DB accessui.


## Backendas

Trumpai apie kai kuriuos backendo sprendimus. Naudojau EF code first ir migrationus. Sakyciau daug paprasciau kurti ir palaikyti vientisuma tarp DB ir projekto.
Pagrinde yra du metodai, skirti grazini produktu lista ir produkto detales. Turbut uzklius View Modeliu formavimas, bet pritruko laiko panaudoti Automaperi, ir tikriausiai nebutu cia kazkoks bonusas ji panaudojus, tai dariau 'old way'. Abu jie grazina partial viewus nes yra kvieciami ajaxo. 
Parodymui turiu pakures extension metoda, kuris is produkto paveiksleliu parenka tinkama pagal enuma, ir sugeneruoja path kur ta paveiksleli gaut. Sitoj vietoj Mes savo patirtyje darydavom panasiai, ir visus img saugodavom azure blob storage.
Product servisas labai paprastas, nes kazkokios biznio logikos kaip ir nera. Tai istraukia viena item, arba grazina daug itemu pagal filtru parametrus. Irgi jokio rocket science:)


## Frontendas

Cia tikriausiai gali kilti daugiausiai klausimu, i kuriuos atsakymas bendrinis butu naudoti SPA :)
Bet kadangi jau jusu siustas pavyzdys buvo su # naudojimu ir routinimu per ji, tai as irgi prisirisau prie jo ir gavosi toks mini SPA.
Viska sumeciau i viena faila, kas aisku nera itin gerai, bet ~150 eiluciu nera daug, ir susiprasti galima, o reikalui esant galeciau isskaidyti. 
Pagrindine ideja kuri tikriausiai pratese jusu minti darant ta js faila, tai buvo ristis prie # urlu, ir viskas vyksta per juos. yra map funkcija, kuri pagal # parametrus parenka tinkamus veiksmus, pvz rodyti 1 produkta ar filtruoti. O toliau jau filtrai tik keicia ta # urla, kuris ant kiekvieno change kviecia map funkcija.


Tai gal viskas ka norejau pasakyti.
**Jei kils klausimu, butinai klauskite :)**